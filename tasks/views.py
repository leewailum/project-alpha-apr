from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.assignee = request.user
            account.save()
            return redirect("projects/detail.html")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    task = Task.objects.filter(assignee=request.user)
    context = {
        "show_my_tasks": task,
    }
    return render(request, "tasks/list_task.html", context)
